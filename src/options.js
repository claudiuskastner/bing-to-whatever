// Saves options to chrome.storage
const save_options = (engine) => {
    chrome.storage.sync.set({['engine']: engine}, () => {
        // Update status to let user know options were saved.
        let status = document.querySelector('#status');
        status.textContent = 'Options saved.';
        setTimeout(() => status.textContent = '', 750);
    });
};
const engineSelected = document.querySelector('#engine');
const restore_options = () => chrome.storage.sync.get('engine',
    e => {
        engineSelected.value = e.engine;
    }
);
document.addEventListener('DOMContentLoaded', restore_options);
engineSelected.addEventListener('change',
    () => save_options(engineSelected.value), false
);
